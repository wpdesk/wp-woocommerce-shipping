<?php


use WPDesk\AbstractShipping\Shipment\Address;
use WPDesk\AbstractShipping\Shipment\Client;
use WPDesk\AbstractShipping\Shipment\Dimensions;
use WPDesk\AbstractShipping\Shipment\Item;
use WPDesk\AbstractShipping\Shipment\Package;
use WPDesk\AbstractShipping\Shipment\Shipment;
use WPDesk\AbstractShipping\Shipment\Weight;

class ShipmentFaker {

	/**
	 * Creates a blank Shipment. Valid but with no data.
	 *
	 * @return Shipment
	 */
	public static function build_blank_shipment() {
		$shipment = new Shipment();

		$package                           = new Package();
		$item                              = new Item();
		$item->name                        = 'Item name';
		$item->dimensions                  = new Dimensions();
		$item->dimensions->dimensions_unit = Dimensions::DIMENSION_UNIT_CM;
		$item->dimensions->length          = 10;
		$item->dimensions->width           = 10;
		$item->dimensions->height          = 10;
		$item->weight                      = new Weight();
		$item->weight->weight              = 1;
		$item->weight->weight_unit         = Weight::WEIGHT_UNIT_KG;
		$package->items                    = [ $item ];
		$shipment->packages                = [ $package ];

		$client              = new Client();
		$client->address     = new Address();
		$shipment->ship_from = $client;
		$shipment->ship_to   = $client;

		return $shipment;
	}

	/**
	 * Creates a Shipment with overweight weight.
	 *
	 * @return Shipment
	 */
	public static function build_overweight_shipment() {
		$shipment = self::build_blank_shipment();
		$shipment->packages[0]->items[0]->weight->weight = 69;
		return $shipment;
	}
}
