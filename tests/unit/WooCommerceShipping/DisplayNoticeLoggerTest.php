<?php


use Psr\Log\NullLogger;
use WPDesk\WooCommerceShipping\DisplayNoticeLogger;

class DisplayNoticeLoggerTest extends \PHPUnit\Framework\TestCase {
	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function test_tries_to_show_when_log() {
		\WP_Mock::userFunction( 'wc_has_notice', array(
			'return' => function () {
				return false;
			}
		) );
		\WP_Mock::userFunction( 'wc_add_notice', array(
			'times' => 2 // add notice shopuld be called twice as two log message will be added
		) );

		$notice_logger = new DisplayNoticeLogger( new NullLogger(), 'test service', 0 );
		$notice_logger->debug( 'whatever message' );
		$notice_logger->alert( 'whatever message' );

		$this->assertTrue(true);
	}
}
