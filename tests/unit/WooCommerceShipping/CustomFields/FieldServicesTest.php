<?php

use WPDesk\WooCommerceShipping\CustomFields\Services\FieldServices;

class FieldServicesTest extends \PHPUnit\Framework\TestCase {
	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function test_can_render_fields() {
		$field    = new FieldServices( array() );

		\WP_Mock::passthruFunction( 'wp_kses_post' );

		$this->assertNotEmpty( $field->render( [
			'type'      => 'whatever',
			'title'     => 'whatever2',
			'field_key' => 'wharever3',
			'options'   => []
		] ), 'Field should render some content' );
	}
}
