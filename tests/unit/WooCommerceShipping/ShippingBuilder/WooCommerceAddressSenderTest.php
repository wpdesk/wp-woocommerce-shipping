<?php


use WPDesk\WooCommerceShipping\ShippingBuilder\WooCommerceAddressSender;

class WooCommerceAddressSenderTest extends \PHPUnit\Framework\TestCase {

	const FIXTURE_ADDRESS_KEY = 'address';
	const FIXTURE_ADDRESS_KEY_2 = 'address2';
	const FIXTURE_CITY_KEY = 'city';
	const FIXTURE_POSTCODE_KEY = 'postcode';
	const FIXTURE_COUNTRY_KEY = 'country';
	const FIXTURE_STATE_KEY = 'state';

	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function test_can_get_valid_address() {
		$address_fixture = [
			self::FIXTURE_ADDRESS_KEY   => 'address value',
			self::FIXTURE_ADDRESS_KEY_2 => 'address value 2',
			self::FIXTURE_CITY_KEY      => 'city value',
			self::FIXTURE_POSTCODE_KEY  => '12-123',
			self::FIXTURE_COUNTRY_KEY   => 'PL',
			self::FIXTURE_STATE_KEY     => 'PL-DS'
		];

		\WP_Mock::userFunction( 'get_option', array(
			'return' => function ( $name, $default ) use ( $address_fixture ) {
				switch ( $name ) {
					case 'woocommerce_store_address':
						return $address_fixture[ self::FIXTURE_ADDRESS_KEY ];
						break;
					case 'woocommerce_store_address_2':
						return $address_fixture[ self::FIXTURE_ADDRESS_KEY_2 ];
						break;
					case 'woocommerce_store_city':
						return $address_fixture[ self::FIXTURE_CITY_KEY ];
						break;
					case 'woocommerce_store_postcode':
						return $address_fixture[ self::FIXTURE_POSTCODE_KEY ];
						break;
					case 'woocommerce_default_country':
						return "{$address_fixture[self::FIXTURE_COUNTRY_KEY]}:{$address_fixture[self::FIXTURE_STATE_KEY ]}";
						break;
				}
			}
		) );

		$address_receiver = new WooCommerceAddressSender();
		$address          = $address_receiver->get_address();

		$this->assertEquals( $address_fixture[ self::FIXTURE_ADDRESS_KEY ], $address->address_line1 );
		$this->assertEquals( $address_fixture[ self::FIXTURE_ADDRESS_KEY_2 ], $address->address_line2 );
		$this->assertEquals( $address_fixture[ self::FIXTURE_CITY_KEY ], $address->city );
		$this->assertEquals( $address_fixture[ self::FIXTURE_POSTCODE_KEY ], $address->postal_code );
		$this->assertEquals( $address_fixture[ self::FIXTURE_COUNTRY_KEY ], $address->country_code );
		$this->assertEquals( $address_fixture[ self::FIXTURE_STATE_KEY ], $address->state_code );
	}
}
