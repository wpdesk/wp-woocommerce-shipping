<?php


use WPDesk\AbstractShipping\Shipment\Dimensions;
use WPDesk\AbstractShipping\Shipment\Shipment;
use WPDesk\AbstractShipping\Shipment\Weight;
use WPDesk\WooCommerceShipping\ShippingBuilder\AddressProvider;
use WPDesk\WooCommerceShipping\ShippingBuilder\WooCommerceShippingBuilder;

class WooCommerceShippingBuilderTest extends \PHPUnit\Framework\TestCase {
	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function test_can_build_shipment() {
		$package      = ['contents' => []];
		$address_from = $this->createMock( AddressProvider::class );
		$address_to   = $this->createMock( AddressProvider::class );

		$builder = new WooCommerceShippingBuilder();
		$builder->set_woocommerce_package( $package );
		$builder->set_sender_address( $address_from );
		$builder->set_receiver_address( $address_to );
		$builder->set_dimension_unit( Dimensions::DIMENSION_UNIT_MM );
		$builder->set_weight_unit( Weight::WEIGHT_UNIT_G );
		$builder->set_currency( 'PLN' );
		$builder->set_rounding_precision( 2 );
		$this->assertInstanceOf( Shipment::class, $builder->build_shipment() );
	}
}
