[![pipeline status](https://gitlab.com/wpdesk/wp-woocommerce-shipping/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-woocommerce-shipping/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-woocommerce-shipping/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-woocommerce-shipping/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping/v/stable)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping/downloads)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping) 
[![License](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping/license)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping) 

# WooCommerce Shipping

The library providing interfaces, abstracts and traits allowing to build complete WooCommerce shipping integrations such as: 
- [UPS Live Rates](https://octolize.com/product/ups-woocommerce-live-rates-and-access-points-pro/)
- [FedEx Live Rates](https://octolize.com/product/fedex-woocommerce-live-rates-pro/)
- [DHL Express Live Rates](https://octolize.com/product/dhl-express-woocommerce-live-rates-pro/)
- [USPS Live Rates](https://octolize.com/product/usps-woocommerce-live-rates-pro/)

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/wp-woocommerce-shipping
```

## Example usage

Used by https://gitlab.com/wpdesk/wp-ups-shipping-method library.
