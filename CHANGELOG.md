## [3.21.2] - 2022-08-30
### Fixed
- de_DE translators

## [3.21.1] - 2022-08-23
### Fixed
- Fatal Error: Uncaught Error: Null get_countries() member function call

## [3.21.0] - 2022-08-16
### Added
- en_CA, en_GB translators

## [3.20.0] - 2022-08-16
### Added
- de_DE translators

## [3.19.0] - 2022-08-09
### Added
- en_AU translators

## [3.18.1] - 2022-06-07
### Added
- cached service
### Updated
- abstract shipping library

## [3.17.0] - 2022-05-22
### Changed
- plugin flow library

## [3.16.1] - 2022-04-15
### Fixed
- null items in package

## [3.16.0] - 2022-04-12
### Changed
- allowed wpdesk/wp-view 2.0

## [3.15.3] - 2022-04-05
### Changed
- added should_calculate_shipping method
- array_merge instead of array sum in settings values
### Fixed
- instance settings

## [3.15.0] - 2022-01-11
### Changed
- services field - field description

## [3.14.1] - 2021-11-15
### Fixed
- filter rates for maximum transit time when delivery dates enabled

## [3.14.0] - 2021-10-28
### Changed
- services field - allow select/deselect all services
- improved notices logger

## [3.13.4] - 2021-10-25
### Changed
- more texts improvements

## [3.13.3] - 2021-10-21
### Changed
- more texts improvements

## [3.13.2] - 2021-10-21
### Changed
- texts improvements

## [3.13.1] - 2021-10-08
### Fixed
- Custom fields values

## [3.13.0] - 2021-09-30
### Added
- Passing a method to filter

## [3.12.3] - 2021-09-29
### Added
- Additional Filter for meta data in other rate methods

## [3.12.2] - 2021-09-29
### Fixed
- Fixing method ID

## [3.12.1] - 2021-09-29
### Added
- Additional Filter for meta data

## [3.12.0] - 2021-03-16
### Changed
- allowed wpdesk/wp-plugin-flow 3.0

## [3.11.0] - 2021-03-16
### Added
- cached collection point provider

## [3.10.2] - 2021-03-15
### Fixed
- invalid collection point when country changed 

## [3.10.1] - 2021-01-12
### Fixed
- Free Shipping price field

## [3.10.0] - 2021-01-05
### Added
- Support for Free Shipping

## [3.9.0] - 2020-08-27
### Fixed
- delivery dates displayed multiple times

## [3.8.3] - 2020-08-27
### Fixed
- Fatal error when no collections point found for given address

## [3.8.2] - 2020-08-27
### Fixed
- Active Payments and rates to collection point 

## [3.8.1] - 2020-08-24
### Fixed
- fallback rate id

## [3.8.0] - 2020-08-24
### Added
- Active Payments Integration class

## [3.7.14] - 2020-07-29
### Fixed
- Searching for collection points based on destination address

## [3.7.13] - 2020-07-23
### Fixed
- Address data with no value from woocommerce_store_* option
### Added
- Complete data for Address object

## [3.7.12] - 2020-07-01
### Added
- Shop price rounding precision in ShopSettings interface

## [3.7.11] - 2020-04-20
### Fixed
- do not try add rate to collection point when CollectionPointNotFoundException 

## [3.7.10] - 2020-04-20
### Fixed
- multiple flat rate in shipping zone

## [3.7.9] - 2020-04-06
### Fixed
- handled CollectionPointNotFoundException

## [3.7.8] - 2020-03-25
### Changed
- translations 

## [3.7.7] - 2020-03-09
### Changed
- added additional security hardenings 

## [3.7.6] - 2020-03-04
### Changed
- fallback debug message 

## [3.7.5] - 2020-02-19
### Added
- more messages in debug mode

## [3.7.4] - 2020-02-12
### Changed
- description in fee value tooltip

## [3.7.3] - 2020-02-11
### Removed
- custom origin settings title

## [3.7.2] - 2020-02-04
### Fixed
- translations

## [3.7.1] - 2020-02-04
### Fixed
- default value in title field - woocommerce notice fixed

## [3.7.0] - 2020-02-04
### Added
- custom origin support

## [3.6.0] - 2020-02-04
### Added
- API status field functionality

## [3.5.7] - 2020-01-31
### Fixed
- force shipping recalculation only when needed

## [3.5.6] - 2020-01-30
### Added
- service code meta data interpreter

## [3.5.5] - 2020-01-30
### Changed
- debug notices button css class

## [3.5.4] - 2020-01-30
### Added
- service name in debug messages

## [3.5.3] - 2020-01-28
### Fixed
- packages names

## [3.5.2] - 2020-01-24
### Fixed
- sidebar width

## [3.5.1] - 2020-01-23
### Fixed
- box name for packer separately

## [3.5] - 2020-01-20
### Added
- package weight calculation

## [3.4.4] - 2020-01-16
### Fixed
- debug messages displays also objects and arrays

## [3.4.3] - 2020-01-16
### Fixed
- can_see_logs method

## [3.4.2] - 2020-01-16
### Added
- translations

## [3.4.1] - 2020-01-16
### Added
- prevent default on debug mode buttons

## [3.4.0] - 2020-01-16
### Added
- debug messages scripts

## [3.3.0] - 2020-01-14
### Added
- item name
- packed packages meta data

## [3.2.7] - 2020-01-14
### Fixed
- method title and title

## [3.2.6] - 2020-01-14
### Added
- support for number settings fields

## [3.2.5] - 2020-01-14
### Fixed
- collection point flat rate

## [3.2.4] - 2020-01-14
### Fixed
- collection point label

## [3.2.3] - 2020-01-14
### Fixed
- fallback method title

## [3.2.2] - 2020-01-10
### Changed
- shipping_builder is protected

## [3.2.1] - 2020-01-08
### Fixed
- SenderAddressTrait use removed
- sender address 

## [3.2.0] - 2020-01-07
### Added
- estimated delivery meta 
- flat rate
### Fixed
- collection points checkout handler

## [3.1.0] - 2019-12-27
### Changed
- CustomFields
- RateMethods
- Settings -> MethodFieldsFactory
- Many little things
- Many TODOS

## [3.0.3] - 2019-12-16
### Added
- handling fees field support

## [3.0.2] - 2019-12-16
### Fixed
- translation text domain

## [3.0.1] - 2019-12-13
### Fixed
- single item value

## [3.0.0] - 2019-12-13
### Changed
- abstract shipping v 2.0.0

## [2.1.7] - 2019-12-11
### Fixed
- custom services field

## [2.1.6] - 2019-12-11
### Changed
- notice formatting

## [2.1.5] - 2019-12-11
### Added
- ability to change sender address

## [2.1.4] - 2019-12-10
### Added
- ShopSetting interface

## [2.1.3] - 2019-12-09
### Added
- JS fallback assets
### Removed
- Fields from SettingsTrait as it fails with PHP Strict Standards

## [2.1.2] - 2019-12-09
- settings defaults

## [2.1.1] - 2019-12-09
### Changed
- Fallback fields
- Metadata interpreters

## [2.1.0] - 2019-12-06
### Added
- Fallback rate method
- RateMethods
- RateWithRateMethodsTrait
### Changed
- Traits moved to ShippingMethod namespace
### Removed
- can_see_logs property

## [2.0.0] - 2019-12-04
### Added
- $this parameter in rate filters
### Changed
- ShippingMethod refactor: logger code moved to logger trait
- Move some of rates functionality to RatesTrait
- ShippingMethod::init changed from private to protected
### Fixed
- wpdesk/abstract-shipping dependency
- ServicesFieldTrait code moved to SettingsTrait
### Removed
- return true from method that can only return true

## [1.2.2] - 2019-12-04
### Changed
- html escaping in debug notices 
### Fixed
- can_see_logs 

## [1.2.1] - 2019-12-03
### Changed
- abstract shipping version

## [1.2.0] - 2019-12-03
### Added
- collection points
- order meta data
- api status field

## [1.1.0] - 2019-11-28
### Added
- declared_value in shipment
### Changed
- WooCommerceShippingBuilder requires currency

## [1.0.7] - 2019-11-18
### Added
- countries and states from WooCommerce

## [1.0.6] - 2019-11-15
### Fixed
- service_id string contains flexible_shipping

## [1.0.5] - 2019-11-15
### Fixed
- removed fedex sidebar
- changed filter name

## [1.0.4] - 2019-11-15
### Fixed
- removed fedex api

## [1.0.3] - 2019-11-14
### Fixed
- translations

## [1.0.2] - 2019-11-14
### Changed
- code refactoring

## [1.0.1] - 2019-11-14
### Added
- Shipping Method

## [1.0.0] - 2019-11-13
### Added
- initial version
