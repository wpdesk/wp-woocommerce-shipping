<?php
/**
 * WooCommerce not initialized Exception.
 *
 * @package WPDesk\WooCommerceShipping
 */

namespace WPDesk\WooCommerceShipping;

use Throwable;

/**
 * WooCommerce not initialized Exception.
 */
class WooCommerceNotInitializedException extends \RuntimeException {
}
