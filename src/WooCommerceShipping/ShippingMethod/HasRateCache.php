<?php
/**
 * Capability: HasRateCache interface.
 *
 * @package WPDesk\WooCommerceShipping\ShippingMethod
 */

namespace WPDesk\WooCommerceShipping\ShippingMethod;

/**
 * Interface for caching rates.
 */
interface HasRateCache {

}
