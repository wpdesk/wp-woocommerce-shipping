<?php
/**
 * Capability: HasFreeShipping interface.
 *
 * @package WPDesk\WooCommerceShipping\ShippingMethod
 */

namespace WPDesk\WooCommerceShipping\ShippingMethod;

/**
 * Interface for free shipping.
 */
interface HasFreeShipping {

}
